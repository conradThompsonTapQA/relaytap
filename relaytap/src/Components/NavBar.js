import React from 'react';
import "./NavBar.css";
import Logo from './tapqa.png';

const NavBar = () => {
    return (
        <div className="nav-container">
            <img src={Logo} className='logo' alt="Logo" />
            <div className="navButtonGrid">
            <a href="#" className="NavBarButton">Place Holder</a>
            <a href="#" className="NavBarButton">Place Holder</a>
            <a href="#" className="NavBarButton">Place Holder</a>
            </div>
        </div>);
}

export default NavBar;