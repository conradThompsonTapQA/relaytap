import React from 'react';
import NavBar from './Components/NavBar.js';
import SideBar from './Components/SideBar.js';
import Dashboard from './Components/Dashboard.js';
import './App.css';

const App = () => {
  return (
    <div> 
      <NavBar></NavBar>
      <SideBar></SideBar>
      <Dashboard></Dashboard>
    </div>);
}

export default App;